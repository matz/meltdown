# meltdown
通过利用meltdown漏洞，在应用层获取Linux内核数据（linux_proc_banner）

Note: Support Linux x64 only

## 如何编译运行?
1. `gcc meldown.c`
2. `./run.sh`

如果一切顺利，你会得到linux_proc_banner的值为: "%s version %s"
![output](output.png)